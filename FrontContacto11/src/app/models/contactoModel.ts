export class ContactoModel{
    id?: number;
    nombre!: string;
    email!: string;
    telefono: string | undefined;
    fecha!: string;
    direccion!: string;

}